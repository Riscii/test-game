define ()->
	#
	# configure lib paths and shims for 3rd party libraries
	#
	require.config
		paths :
			
			#Gamecore
			"gamecore/gamecore"   : "/components/playcraftjs/lib/ext/gamecore.js/src/gamecore"
			"gamecore/class"      : "/components/playcraftjs/lib/ext/gamecore.js/src/class"
			"gamecore/jhashtable" : "/components/playcraftjs/lib/ext/gamecore.js/src/jhashtable"
			"gamecore/device"     : "/components/playcraftjs/lib/ext/gamecore.js/src/device"
			"gamecore/perf"       : "/components/playcraftjs/lib/ext/gamecore.js/src/perf"
			"gamecore/linkedlist" : "/components/playcraftjs/lib/ext/gamecore.js/src/linkedlist"
			"gamecore/hashlist"   : "/components/playcraftjs/lib/ext/gamecore.js/src/hashlist"
			"gamecore/stacktrace" : "/components/playcraftjs/lib/ext/gamecore.js/src/stacktrace"
			"gamecore"            : "/components/playcraftjs/lib/ext/gamecore.js/src/pooled"

			#Playcraft
			"playcraft/playcraft" : "/components/playcraftjs/lib/playcraft"

			"playcraft/boot"         : "/components/playcraftjs/lib/boot"
			"playcraft/input"        : "/components/playcraftjs/lib/input"
			"playcraft/hashmap"      : "/components/playcraftjs/lib/hashmap"
			"playcraft/tools"        : "/components/playcraftjs/lib/tools"
			"playcraft/color"        : "/components/playcraftjs/lib/color"
			"playcraft/debug"        : "/components/playcraftjs/lib/debug"
			"playcraft/device"       : "/components/playcraftjs/lib/device"
			"playcraft/sound"        : "/components/playcraftjs/lib/sound"
			"playcraft/layer"        : "/components/playcraftjs/lib/layer"
			"playcraft/entitylayer"  : "/components/playcraftjs/lib/entitylayer"
			"playcraft/tileset"      : "/components/playcraftjs/lib/tileset"
			"playcraft/tilemap"      : "/components/playcraftjs/lib/tilemap"
			"playcraft/tilelayer"    : "/components/playcraftjs/lib/tilelayer"
			"playcraft/entity"       : "/components/playcraftjs/lib/entity"
			"playcraft/sprite"       : "/components/playcraftjs/lib/sprite"
			"playcraft/spritesheet"  : "/components/playcraftjs/lib/spritesheet"
			"playcraft/math"         : "/components/playcraftjs/lib/math"
			"playcraft/image"        : "/components/playcraftjs/lib/image"
			"playcraft/scene"        : "/components/playcraftjs/lib/scene"
			"playcraft/game"         : "/components/playcraftjs/lib/game"
			"playcraft/loader"       : "/components/playcraftjs/lib/loader"
			"playcraft/dataresource" : "/components/playcraftjs/lib/dataresource"

			"playcraft/components/component"       : "/components/playcraftjs/lib/components/component"
			"playcraft/components/physics"         : "/components/playcraftjs/lib/components/physics"
			"playcraft/components/alpha"           : "/components/playcraftjs/lib/components/alpha"
			"playcraft/components/joint"           : "/components/playcraftjs/lib/components/joint"
			"playcraft/components/expiry"          : "/components/playcraftjs/lib/components/expiry"
			"playcraft/components/originshifter"   : "/components/playcraftjs/lib/components/originshifter"
			"playcraft/components/spatial"         : "/components/playcraftjs/lib/components/spatial"
			"playcraft/components/overlay"         : "/components/playcraftjs/lib/components/overlay"
			"playcraft/components/clip"            : "/components/playcraftjs/lib/components/clip"
			"playcraft/components/activator"       : "/components/playcraftjs/lib/components/activator"
			"playcraft/components/input"           : "/components/playcraftjs/lib/components/input"
			"playcraft/components/fade"            : "/components/playcraftjs/lib/components/fade"
			"playcraft/components/rect"            : "/components/playcraftjs/lib/components/rect"
			"playcraft/components/text"            : "/components/playcraftjs/lib/components/text"
			"playcraft/components/sprite"          : "/components/playcraftjs/lib/components/sprite"
			"playcraft/components/layout"          : "/components/playcraftjs/lib/components/layout"
			"playcraft/components/particleemitter" : "/components/playcraftjs/lib/components/particleemitter"

			"playcraft/es/entitymanager" : "/components/playcraftjs/lib/es/entitymanager"
			"playcraft/es/systemmanager" : "/components/playcraftjs/lib/es/systemmanager"

			"playcraft/systems/system"       : "/components/playcraftjs/lib/systems/system"
			"playcraft/systems/entitysystem" : "/components/playcraftjs/lib/systems/entitysystem"
			"playcraft/systems/physics"      : "/components/playcraftjs/lib/systems/physics"
			"playcraft/systems/effects"      : "/components/playcraftjs/lib/systems/effects"
			"playcraft/systems/particles"    : "/components/playcraftjs/lib/systems/particles"
			"playcraft/systems/input"        : "/components/playcraftjs/lib/systems/input"
			"playcraft/systems/expiry"       : "/components/playcraftjs/lib/systems/expiry"
			"playcraft/systems/activation"   : "/components/playcraftjs/lib/systems/activation"
			"playcraft/systems/render"       : "/components/playcraftjs/lib/systems/render"
			"playcraft"                      : "/components/playcraftjs/lib/systems/layout"

			#Other
			"jquery"    : "/components/playcraftjs/lib/ext/jquery171"
			"base64"    : "/components/playcraftjs/lib/ext/base64"
			"box2dweb"  : "/components/playcraftjs/lib/ext/box2dweb.2.1a-pc"
			
		shim :
			"gamecore/gamecore"   : [ "gamecore/class" ]
			"gamecore/class"      : [ "jquery" ]
			"gamecore/jhashtable" : [ "gamecore/gamecore" ]
			"gamecore/device"     : [ "gamecore/gamecore" ]
			"gamecore/perf"       : [ "gamecore/gamecore" ]
			"gamecore/linkedlist" : [ "gamecore/gamecore" ]
			"gamecore/hashlist"   : [ "gamecore/gamecore" ]
			"gamecore/stacktrace" : [ "gamecore/gamecore" ]
			"gamecore"            : [ 
				"gamecore/class"
				"gamecore/gamecore"
				"gamecore/jhashtable"
				"gamecore/device"
				"gamecore/perf"
				"gamecore/linkedlist"
				"gamecore/hashlist"
				"gamecore/stacktrace"
			]

			"playcraft/boot"         : [ "gamecore", "playcraft/playcraft" ]    
			"playcraft/input"        : [ "gamecore", "playcraft/playcraft" ]  
			"playcraft/hashmap"      : [ "gamecore", "playcraft/playcraft" ] 
			"playcraft/tools"        : [ "gamecore", "playcraft/playcraft" ] 
			"playcraft/color"        : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/debug"        : [ "gamecore", "playcraft/playcraft" ] 
			"playcraft/device"       : [ "gamecore", "playcraft/playcraft" ] 
			"playcraft/sound"        : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/layer"        : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/entitylayer"  : [ "gamecore", "playcraft/playcraft", "playcraft/layer" ]
			"playcraft/tileset"      : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/tilemap"      : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/tilelayer"    : [ "gamecore", "playcraft/playcraft", "playcraft/layer" ]
			"playcraft/entity"       : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/sprite"       : [ "gamecore", "playcraft/playcraft" ]  
			"playcraft/spritesheet"  : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/math"         : [ "gamecore", "playcraft/playcraft" ] 
			"playcraft/image"        : [ "gamecore", "playcraft/playcraft" ]  
			"playcraft/scene"        : [ "gamecore", "playcraft/playcraft" ]  
			"playcraft/game"         : [ "gamecore", "playcraft/playcraft" ]  
			"playcraft/loader"       : [ "gamecore", "playcraft/playcraft" ] 
			"playcraft/dataresource" : [ "gamecore", "playcraft/playcraft" ]

			"playcraft/components/component"       : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/components/physics"         : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/alpha"           : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/joint"           : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/expiry"          : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/originshifter"   : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/spatial"         : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/overlay"         : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/clip"            : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/activator"       : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/input"           : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/fade"            : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/rect"            : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/text"            : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/sprite"          : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/layout"          : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]
			"playcraft/components/particleemitter" : [ "gamecore", "playcraft/playcraft", "playcraft/components/component" ]

			"playcraft/es/entitymanager" : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/es/systemmanager" : [ "gamecore", "playcraft/playcraft" ]

			"playcraft/systems/system"       : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/systems/entitysystem" : [ "gamecore", "playcraft/playcraft" ]
			"playcraft/systems/physics"      : [ "gamecore", "playcraft/playcraft", "playcraft/components/physics" ]
			"playcraft/systems/effects"      : [ "gamecore", "playcraft/playcraft", "playcraft/systems/entitysystem" ]
			"playcraft/systems/particles"    : [ "gamecore", "playcraft/playcraft", "playcraft/systems/entitysystem" ]
			"playcraft/systems/input"        : [ "gamecore", "playcraft/playcraft", "playcraft/systems/entitysystem" ]
			"playcraft/systems/expiry"       : [ "gamecore", "playcraft/playcraft", "playcraft/systems/entitysystem" ]
			"playcraft/systems/activation"   : [ "gamecore", "playcraft/playcraft", "playcraft/systems/entitysystem" ]
			"playcraft/systems/render"       : [ "gamecore", "playcraft/playcraft", "playcraft/systems/entitysystem" ]               

			"playcraft"              : [ 
				"gamecore"

				"playcraft/boot"
				"playcraft/playcraft"        
				"playcraft/input"       
				"playcraft/hashmap"     
				"playcraft/tools"       
				"playcraft/color"       
				"playcraft/debug"       
				"playcraft/device"      
				"playcraft/sound"       
				"playcraft/layer"       
				"playcraft/entitylayer" 
				"playcraft/tileset"     
				"playcraft/tilemap"     
				"playcraft/tilelayer"   
				"playcraft/entity"      
				"playcraft/sprite"      
				"playcraft/spritesheet" 
				"playcraft/math"        
				"playcraft/image"       
				"playcraft/scene"       
				"playcraft/game"        
				"playcraft/loader"      
				"playcraft/dataresource"

				"playcraft/components/component"      
				"playcraft/components/physics"        
				"playcraft/components/alpha"          
				"playcraft/components/joint"          
				"playcraft/components/expiry"         
				"playcraft/components/originshifter"  
				"playcraft/components/spatial"        
				"playcraft/components/overlay"        
				"playcraft/components/clip"           
				"playcraft/components/activator"      
				"playcraft/components/input"          
				"playcraft/components/fade"           
				"playcraft/components/rect"           
				"playcraft/components/text"           
				"playcraft/components/sprite"         
				"playcraft/components/layout"         
				"playcraft/components/particleemitter"

				"playcraft/es/entitymanager"
				"playcraft/es/systemmanager"

				"playcraft/systems/system"       
				"playcraft/systems/entitysystem" 
				"playcraft/systems/physics"      
				"playcraft/systems/effects"      
				"playcraft/systems/particles"    
				"playcraft/systems/input"        
				"playcraft/systems/expiry"       
				"playcraft/systems/activation"   
				"playcraft/systems/render"       
			]




