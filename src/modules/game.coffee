define [
	'playcraft'
],()->

	GameScene = pc.Scene.extend 'GameScene', {},
		gameLayer : null
		box : null

		init: ->
			@_super();

			@gameLayer = @addLayer new pc.EntityLayer('game layer',10000,1000)

			@gameLayer.addSystem new pc.systems.Render()
			@box = pc.Entity.create @gameLayer

			@box.addComponent pc.components.Rect.create
				color:'#ff2222'

			@box.addComponent pc.components.Spatial.create
				x:100
				y:100
				w:50
				h:50

			pc.device.input.bindAction @,'move','SPACE'

		onAction: (actionName, event, pos)->
			if actionName == 'move'
				this.box.getComponent('spatial').pos.x += 10

		process: ->
			pc.device.ctx.clearRect 0, 0, pc.device.canvasWidth, pc.device.canvasHeight
			@_super()

	window.TheGame = pc.Game.extend 'TheGame', {},
		gameScende: null
		onReady: ->
			@_super()

			if pc.device.devMode
				pc.device.loader.setDisableCache()

			@gameScene = new GameScene()
			@addScene this.gameScene

		onLoading:(percentageComplete)->
			#hey bro

		onLoaded:->
			@gameScene = new GameScene()
			@addScene this.gameScene

	pc.device.boot("mycanvas", "TheGame");


