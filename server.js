var express = require('express');
var app = express();

//expose the lib dir to http
app.use(express.static('./lib'));
//expose the components dir to http
app.use('/components',express.static('./components'));

var port = process.env.PORT || 9090;

console.log("Started server on port " + port);

app.listen(port);